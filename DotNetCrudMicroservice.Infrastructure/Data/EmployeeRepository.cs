using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using DotNetCrudMicroservice.Core.Interfaces.Repositories;
using DotNetCrudMicroservice.Core.Models;

namespace DotNetCrudMicroservice.Infrastructure.Data
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly AppDbContext _dbContext;

        public EmployeeRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Employee UpdateEmployee(Employee employee)
        {

            _dbContext.Entry(employee).State = EntityState.Modified;
            _dbContext.SaveChanges();

            return employee;

        }

        public Employee CreateEmployee(Employee employee)
        {

            _dbContext.Add(employee);
            _dbContext.SaveChanges();

            return employee;

        }

        public Employee DeleteEmployee(string firstName)
        {

            var account = _dbContext.Employee.Find(firstName);
            _dbContext.Employee.Remove(account);
            _dbContext.SaveChanges();

            return null;

        }

        public List<Employee> GetAllEmployees()
        {

            return _dbContext.Employee.ToList();

        }

        public Employee GetEmployee(string firstName)
        {

            return _dbContext.Employee.Find(firstName);

        }

    }
}
