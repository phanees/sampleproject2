using System;
using System.Collections.Generic;
using DotNetCrudMicroservice.Core.Interfaces.Services;
using DotNetCrudMicroservice.Core.Interfaces.Repositories;


using DotNetCrudMicroservice.Core.Models;

namespace DotNetCrudMicroservice.Core.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public Employee UpdateEmployee(Employee employee)
        {
            return _employeeRepository.UpdateEmployee(employee);
        }

        public Employee CreateEmployee(Employee employee)
        {
            return _employeeRepository.CreateEmployee(employee);
        }

        public Employee DeleteEmployee(string firstName)
        {
            return _employeeRepository.DeleteEmployee(firstName);
        }

        public List<Employee> GetAllEmployees()
        {
            return _employeeRepository.GetAllEmployees();
        }

        public Employee GetEmployee(string firstName)
        {
            return _employeeRepository.GetEmployee(firstName);
        }

    }
}
