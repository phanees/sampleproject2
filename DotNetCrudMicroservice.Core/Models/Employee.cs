using System;

namespace DotNetCrudMicroservice.Core.Models
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int Pincode { get; set; }
    }
}