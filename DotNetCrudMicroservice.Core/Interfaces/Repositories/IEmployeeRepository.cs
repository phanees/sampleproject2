using System;
using System.Collections.Generic;


using DotNetCrudMicroservice.Core.Models;

namespace DotNetCrudMicroservice.Core.Interfaces.Repositories
{
    public interface IEmployeeRepository
    {

        Employee UpdateEmployee(Employee employee);

        Employee CreateEmployee(Employee employee);

        Employee DeleteEmployee(string firstName);

        List<Employee> GetAllEmployees();

        Employee GetEmployee(string firstName);

    }
}
