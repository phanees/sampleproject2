using System.Transactions;
using Microsoft.AspNetCore.Mvc;
using DotNetCrudMicroservice.Core.Interfaces.Services;


using DotNetCrudMicroservice.Core.Models;
using Microsoft.Extensions.Logging;
using System;

namespace DotNetCrudMicroservice.Controllers
{
    public class EmployeeController : BaseApiController
    {
        private readonly IEmployeeService _employeeService;
        private readonly ILogger<EmployeeController> _logger;

        public EmployeeController(IEmployeeService employeeService, ILogger<EmployeeController> logger)
        {
            _employeeService = employeeService;
            _logger = logger;
        }

        [HttpPut("/employee")]
        public IActionResult UpdateEmployee([FromBody] Employee employee)
        {
            _logger.LogInformation("New request for UpdateEmployee");

            try
            {
                using (var scope = new TransactionScope())
                {
                    var response = _employeeService.UpdateEmployee(employee);
                    scope.Complete();
                    return new OkObjectResult(response);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        [HttpPost("/employee")]
        public IActionResult CreateEmployee([FromBody] Employee employee)
        {
            _logger.LogInformation("New request for CreateEmployee");

            try
            {
                using (var scope = new TransactionScope())
                {
                    var response = _employeeService.CreateEmployee(employee);
                    scope.Complete();
                    return new OkObjectResult(response);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        [HttpDelete("/employee")]
        public IActionResult DeleteEmployee(string firstName)
        {
            _logger.LogInformation("New request for DeleteEmployee");

            try
            {
                _employeeService.DeleteEmployee(firstName);
                return new OkResult();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        [HttpGet("/employee")]
        public IActionResult GetAllEmployees()
        {
            _logger.LogInformation("New request for GetAllEmployees");

            try
            {
                var response = _employeeService.GetAllEmployees();
                return new OkObjectResult(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        [HttpGet("/employee/{firstName}")]
        public IActionResult GetEmployee(string firstName)
        {
            _logger.LogInformation("New request for GetEmployee");

            try
            {
                var response = _employeeService.GetEmployee(firstName);
                return new OkObjectResult(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

    }
}
