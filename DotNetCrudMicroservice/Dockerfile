FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["DotNetCrudMicroservice/DotNetCrudMicroservice.csproj", "DotNetCrudMicroservice/"]
COPY ["DotNetCrudMicroservice.Core/DotNetCrudMicroservice.Core.csproj", "DotNetCrudMicroservice.Core/"]
COPY ["DotNetCrudMicroservice.Infrastructure/DotNetCrudMicroservice.Infrastructure.csproj", "DotNetCrudMicroservice.Infrastructure/"]
RUN dotnet restore "DotNetCrudMicroservice/DotNetCrudMicroservice.csproj"
COPY . .
WORKDIR "/src/DotNetCrudMicroservice"
RUN dotnet build "DotNetCrudMicroservice.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DotNetCrudMicroservice.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DotNetCrudMicroservice.dll"]
