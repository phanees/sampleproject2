using System;
using System.Collections.Generic;
using Moq;
using DotNetCrudMicroservice.Core.Interfaces.Repositories;


using DotNetCrudMicroservice.Core.Models;
using DotNetCrudMicroservice.Core.Services;

namespace DotNetCrudMicroservice.UnitTests.Core.Services
{
    public class BaseEfRepoTestFixture
    {

        public EmployeeService EmployeeService;

        public Employee Employee;

        public List<Employee> EmployeeList;

        public BaseEfRepoTestFixture()
        {

            Employee = new Employee
            {

                FirstName = "test data",

                LastName = "test data",

                Address = "test data",

                Pincode = 100

            };

            EmployeeList = new List<Employee> {new Employee
                {

                    FirstName =  "test data",

                    LastName =  "test data",

                    Address =  "test data",

                    Pincode = 100

                }
                };

            var employeeRepository = new Mock<IEmployeeRepository>();

            employeeRepository.Setup(x => x.UpdateEmployee(Employee)).Returns(Employee);

            employeeRepository.Setup(x => x.CreateEmployee(Employee)).Returns(Employee);

            employeeRepository.Setup(x => x.DeleteEmployee("test data")).Returns(Employee);

            employeeRepository.Setup(x => x.GetAllEmployees()).Returns(EmployeeList);

            employeeRepository.Setup(x => x.GetEmployee("test data")).Returns(Employee);

            EmployeeService = new EmployeeService(employeeRepository.Object);

        }
    }
}
