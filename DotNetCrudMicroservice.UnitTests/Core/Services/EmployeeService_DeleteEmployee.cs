using System;
using Xunit;
using Newtonsoft.Json;

namespace DotNetCrudMicroservice.UnitTests.Core.Services
{
    public class EmployeeService_DeleteEmployee : IClassFixture<BaseEfRepoTestFixture>
    {
        BaseEfRepoTestFixture _fixture;

        public EmployeeService_DeleteEmployee(BaseEfRepoTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void DeleteEmployee()
        {

            var actual = _fixture.EmployeeService.DeleteEmployee("test data");
            Assert.Equal(JsonConvert.SerializeObject(_fixture.Employee), JsonConvert.SerializeObject(actual));

        }
    }
}
