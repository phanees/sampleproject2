using System;
using Xunit;
using Newtonsoft.Json;

namespace DotNetCrudMicroservice.UnitTests.Core.Services
{
    public class EmployeeService_UpdateEmployee : IClassFixture<BaseEfRepoTestFixture>
    {
        BaseEfRepoTestFixture _fixture;

        public EmployeeService_UpdateEmployee(BaseEfRepoTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void UpdateEmployee()
        {

            var actual = _fixture.EmployeeService.UpdateEmployee(_fixture.Employee);
            Assert.Equal(JsonConvert.SerializeObject(_fixture.Employee), JsonConvert.SerializeObject(actual));

        }
    }
}
