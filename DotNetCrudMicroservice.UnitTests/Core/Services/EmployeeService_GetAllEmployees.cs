using System;
using Xunit;
using Newtonsoft.Json;

namespace DotNetCrudMicroservice.UnitTests.Core.Services
{
    public class EmployeeService_GetAllEmployees : IClassFixture<BaseEfRepoTestFixture>
    {
        BaseEfRepoTestFixture _fixture;

        public EmployeeService_GetAllEmployees(BaseEfRepoTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void GetAllEmployees()
        {

            var actual = _fixture.EmployeeService.GetAllEmployees();
            Assert.Equal(JsonConvert.SerializeObject(_fixture.EmployeeList), JsonConvert.SerializeObject(actual));

        }
    }
}
