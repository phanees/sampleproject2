using System;
using Xunit;
using Newtonsoft.Json;

namespace DotNetCrudMicroservice.UnitTests.Core.Services
{
    public class EmployeeService_CreateEmployee : IClassFixture<BaseEfRepoTestFixture>
    {
        BaseEfRepoTestFixture _fixture;

        public EmployeeService_CreateEmployee(BaseEfRepoTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void CreateEmployee()
        {

            var actual = _fixture.EmployeeService.CreateEmployee(_fixture.Employee);
            Assert.Equal(JsonConvert.SerializeObject(_fixture.Employee), JsonConvert.SerializeObject(actual));

        }
    }
}
