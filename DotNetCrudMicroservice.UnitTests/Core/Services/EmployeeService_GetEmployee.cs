using System;
using Xunit;
using Newtonsoft.Json;

namespace DotNetCrudMicroservice.UnitTests.Core.Services
{
    public class EmployeeService_GetEmployee : IClassFixture<BaseEfRepoTestFixture>
    {
        BaseEfRepoTestFixture _fixture;

        public EmployeeService_GetEmployee(BaseEfRepoTestFixture fixture)
        {
            _fixture = fixture;
        }

        [Fact]
        public void GetEmployee()
        {

            var actual = _fixture.EmployeeService.GetEmployee("test data");
            Assert.Equal(JsonConvert.SerializeObject(_fixture.Employee), JsonConvert.SerializeObject(actual));

        }
    }
}
